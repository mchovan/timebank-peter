from flask import request, jsonify
from sqlalchemy.exc import IntegrityError
from timebank.models.services_model import Service
from timebank import app, db
from timebank.libs.response_helpers import record_sort_params_handler, get_all_db_objects, is_number, ValidationError, \
    user_exists
from sqlalchemy import text

from sqlalchemy import func


from faker import Faker
fake = Faker()


@app.route('/api/v1/services', methods=['GET'])
def api_services():
    sort_field, sort_dir, valid = record_sort_params_handler(request.args, Service)
    if not valid:
        return '', 400
    db_objs = get_all_db_objects(sort_field, sort_dir, db.session.query(Service)).all()

    if len(db_objs):
        response_obj = []
        for obj in db_objs:
            response_obj.append(dict(
                id=obj.id,
                title=obj.title,
                avg_rating=obj.avg_rating,
                User=dict(
                    id=obj.User.id,
                    phone=obj.User.phone,
                    user_name=obj.User.user_name,
                    time_account=obj.User.time_account,
                ),
            ))

        return jsonify(response_obj), 200
    else:
        return {"Message": "No services in database!!!"}, 404


@app.route('/api/v1/service/<services_id>', methods=['GET'])
def api_single_service_get(services_id):

    db_query = db.session.query(Service)
    obj = db_query.get(services_id)

    if not obj:
        return '', 400

    response_obj = [dict(
        id=obj.id,
        title=obj.title,
        avg_rating=obj.avg_rating,
        User=dict(
            id=obj.User.id,
            phone=obj.User.phone,
            user_name=obj.User.user_name,
            time_account=obj.User.time_account,
        ),
    )]

    response = jsonify(response_obj)
    return response, 200


@app.route('/api/v1/service/<services_id>', methods=['PUT'])
def api_single_service_put(services_id):
    if not services_id and type(services_id) is int and 0 < services_id:
        return '', 400

    db_query = db.session.query(Service)
    db_obj = db_query.get(services_id)

    if not db_obj:
        return '', 404

    req_data = None
    if request.content_type == 'application/json':
        req_data = request.json
    elif request.content_type == 'application/x-www-form-urlencoded':
        req_data = request.form

    if 'user_id' in req_data:
        try:
            is_number(req_data['user_id'])
            user_exists(req_data['user_id'])
        except ValidationError:
            return '', 400

        db_obj.user_id = int(req_data['user_id'])

    if 'title' in req_data:
        db_obj.title = req_data['title']

    try:
        db.session.commit()
        db.session.refresh(db_obj)
    except IntegrityError as e:
        return jsonify({'error': str(e.orig)}), 405

    return '', 204


@app.route('/api/v1/service/<services_id>', methods=['DELETE'])
def api_single_service_delete(services_id):

    if not services_id and type(services_id) is int and 0 < services_id:
        return '', 400

    db_query = db.session.query(Service)
    db_test = db_query.get(services_id)
    db_obj = db_query.filter_by(id=services_id)

    if not db_test:
        return '', 404

    try:
        db_obj.delete()
        db.session.commit()
    except IntegrityError as e:
        return jsonify({'error': str(e.orig)}), 405
    else:
        return '', 204


@app.route('/api/v1/service-create', methods=['POST'])
def api_single_service_create():
    db_obj = Service()

    req_data = None
    if request.content_type == 'application/json':
        req_data = request.json
    elif request.content_type == 'application/x-www-form-urlencoded':
        req_data = request.form

    if not req_data['estimate']:
        return {"Message": "Pozor!!"}, 404

    try:
        is_number(req_data['user_id'])
        user_exists(req_data['user_id'])
        is_number(req_data['estimate'])
    except ValidationError as e:
        return jsonify({'error': str(e)}), 400
    db_obj.user_id = int(req_data['user_id'])
    db_obj.title = req_data['title']
    db_obj.estimate = req_data['estimate']

    try:
        db.session.add(db_obj)
        print(db_obj)
        db.session.commit()
        db.session.refresh(db_obj)
        print(db_obj)
    except IntegrityError as e:
        return jsonify({'error': str(e.orig)}), 405

    return '', 201


@app.route('/api/v1/service-create-random', methods=['POST'])
def api_random_service_create():

    db_query = db.session.query(Service)
    db_obj = db_query.order_by(func.random()).first()
    user_id = db_obj.user_id

    my_title = fake.user_name()

    response = {
        "id": user_id+1,
        'title': my_title
    }

    db_obj.user_id = response['id']
    db_obj.title = response['title']

    if not db_obj:
        return '{"message":"no object generated!"}', 400

    try:
        db.session.add(db_obj)
        db.session.commit()
        db.session.refresh(db_obj)

    except IntegrityError as e:
        return jsonify({'error': str(e.orig)}), 405

    db_obj.user_id = int(response['id'])
    db_obj.title = response['title']

    return '{"message": "random user created!"}', 201


# metoda pro autocomplete - /api/v1/service-search?ord=asc&field=title&s=tit
@app.route('/api/v1/serv-srch', methods=['GET'])
def get_service():

    language = request.args.get('Jazyk')
    country = request.args.get('Krajina')
    search = request.args.get('bla')

    db_service = db.session.query(Service)
    db_filter = db_service.filter(Service.title.like(search+'%'))
    print(db_filter)
    hocico = db_filter.all()
    print(hocico)
    res = []
    for x in hocico:
        res.append(dict(
            kvak=x.title
        ))
    return jsonify(res)


# metoda pro autocomplete - /api/v1/service-search?ord=asc&field=title&s=tit
@app.route('/api/v1/service-search', methods=['GET'])
def api_service_search():
    sort_field, sort_dir, valid = record_sort_params_handler(request.args, Service)
    if not valid:
        return {"Message": "blabla"}, 400

    search = request.args.get('search')
    result = []
    if len(search) >= 0:

        db_query = db.session.query(Service)
        db_filter = db_query.filter(Service.title.like(search + '%'))

        if sort_field and sort_dir:
            db_obj = db_filter.order_by(text(sort_field + ' ' + sort_dir)).all()
        else:
            db_obj = db_filter.all()

        for x in db_obj:
            result.append(dict(
                id=x.id,
                title=x.title,
                User=dict(
                    id=x.User.id,
                    phone=x.User.phone,
                    user_name=x.User.user_name,
                    time_account=x.User.time_account,
                ),
                avg_rating=x.avg_rating,
                estimate=x.estimate,
            ))

    return jsonify(result), 200


# Vyhladaj z databazy serviceregister podla ratingu ktore sme zadali!!!
@app.route('/api/v1/find_service_rating', methods=['GET'])
def find_service_rating():

    search = request.args.get('s')
    serviceregister_model = db.session.query(Service)

    try:
        is_number(search)
    except ValidationError as e:
        return jsonify({'error': str(e)}), 400

    sort_field, sort_dir, valid = record_sort_params_handler(request.args, Service)
    if not valid:
        return '', 400
    db_objs = get_all_db_objects(sort_field, sort_dir, db.session.query(Service)).all()

    result = []

    for x in db_objs:
        if x.avg_rating:
            if x.avg_rating >= int(search):
                result.append(dict(
                    id=x.id,
                    title=x.title,
                    user_id=x.user_id,
                    estiamte=x.estimate,
                    avg_rating=x.avg_rating,
                ))

    if result:
        return jsonify(result), 200

    return jsonify({"message": f"No services with {search} and higher rating!"}), 400
