from flask import request, jsonify
from flask_jwt_extended import create_access_token, create_refresh_token, set_access_cookies, set_refresh_cookies, \
    jwt_required, get_jwt_identity, unset_jwt_cookies
from sqlalchemy.exc import IntegrityError, NoResultFound
from werkzeug.security import generate_password_hash, check_password_hash

from timebank.models.users_model import User
from timebank.models.services_model import Service
from timebank import app, db
from timebank.libs.response_helpers import record_sort_params_handler, get_all_db_objects, is_number, ValidationError, \
    phone_number_match

from faker import Faker
from sqlalchemy import text

fake = Faker()

@app.route('/api/v1/users', methods=['GET'])
def api_users():
    sort_field, sort_dir, valid = record_sort_params_handler(request.args, User)
    if not valid:
        return '', 400
    db_objs = get_all_db_objects(sort_field, sort_dir, db.session.query(User)).all()
    print(db_objs)
    # Pokial je aspon jeden objekt v db_objs prejdeme podmienkou
    if len(db_objs):
        response_obj = []
        for obj in db_objs:
            # Do prazdneho listu response_obj pridavame vsetky dictionaries ktore mame v db_objs
            response_obj.append(dict(
                id=obj.id,
                phone=obj.phone,
                user_name=obj.user_name,
                time_account=obj.time_account
            ))
        return jsonify(response_obj), 200
    else:
        return '{"message": "No user in database."}', 404


@app.route('/api/v1/user/<user_id>', methods=['GET'])
def api_single_user_get(user_id):

    # Vytiahnem z databazy model User
    db_query = db.session.query(User)
    # do premennej obj si ulozim konkretneho pouzivatela podla user_id
    obj = db_query.get(user_id)

    if not obj:
        return '', 404

    response_obj = [dict(
        id=obj.id,
        phone=obj.phone,
        user_name=obj.user_name,
        time_account=obj.time_account,
    )]

    response = jsonify(response_obj)
    return response, 200


@app.route('/api/v1/user/<user_id>', methods=['PUT'])
def api_single_user_put(user_id):
    if not user_id and type(user_id) is int and 0 < user_id:
        return '', 400

    db_query = db.session.query(User)
    db_obj = db_query.get(user_id)

    if not db_obj:
        return '', 404

    req_data = None
    if request.content_type == 'application/json':
        req_data = request.json
    elif request.content_type == 'application/x-www-form-urlencoded':
        req_data = request.form

    if 'phone' in req_data:
        try:
            phone_number_match(req_data['phone'])
            db_obj.phone = req_data['phone']
        except ValidationError as e:
            return jsonify({'error': str(e)}), 400

    if 'user_name' in req_data:
        db_obj.user_name = req_data['user_name']

    if 'password' in req_data:
        db_obj.password = generate_password_hash(req_data['password'])

    if 'time_account' in req_data:
        try:
            is_number(req_data['time_account'])
        except ValidationError as e:
            return jsonify({'error': str(e)}), 400

        db_obj.time_account = int(req_data['time_account'])

    try:
        db.session.commit()
        db.session.refresh(db_obj)
    except IntegrityError as e:
        return jsonify({'error': str(e.orig)}), 405

    return '', 204


@app.route('/api/v1/user/<user_id>', methods=['DELETE'])
def api_single_user_delete(user_id):

    if not user_id and type(user_id) is int and 0 < user_id:
        return '', 400

    db_query = db.session.query(User)
    db_test = db_query.get(user_id)
    db_obj = db_query.filter_by(id=user_id)

    if not db_test:
        return '', 404

    try:
        db_obj.delete()
        db.session.commit()
    except IntegrityError as e:
        return jsonify({'error': str(e.orig)}), 405
    else:
        return '', 204


@app.route('/api/v1/user-create', methods=['POST'])
def api_single_user_create():

    db_obj = User()
    print(db_obj)
    req_data = None
    if request.content_type == 'application/json':
        req_data = request.json
    elif request.content_type == 'application/x-www-form-urlencoded':
        req_data = request.form
    try:
        phone_number_match(req_data['phone'])
    except ValidationError as e:
        return jsonify({'error': str(e)}), 400

    if req_data['password'] != req_data['password2']:
        return {'Message': "Both passwords must be the same!!"}, 400

    db_obj.phone = req_data['phone']
    db_obj.password = generate_password_hash(req_data['password'])
    db_obj.user_name = req_data['user_name']
    db_obj.time_account = 0

    try:
        db.session.add(db_obj)
        db.session.commit()
        db.session.refresh(db_obj)
    except IntegrityError as e:
        return jsonify({'error': str(e.orig)}), 405

    return '', 201


# generuj cisla v spravnom formate
# vrat chybu v nejakom formate
@app.route('/api/v1/gen-ran-user', methods=['POST'])
def random_user_create():
    db_obj = User()
    req_data = None

    if request.content_type == 'application/json':
        req_data = request.json
    elif request.content_type == 'application/x-www-form-urlencoded':
        req_data = request.form

    try:
        phone_number_match(req_data['phone'])
    except ValidationError as e:
        return jsonify({'error': str(e)}), 400

    user_name = fake.user_name()
    response = {
        "user_name": user_name
    }
    req_data = request.json

    db_obj.phone = req_data['phone']
    db_obj.password = generate_password_hash(req_data['password'])
    db_obj.user_name = response['user_name']
    db_obj.time_account = 0
    # return name and email as a JSON httpresponse using jsonify

    if not db_obj:
        return '{"message":"no object generated!"}', 400

    try:
        db.session.add(db_obj)
        db.session.commit()
        db.session.refresh(db_obj)

    except IntegrityError as e:
        return jsonify({'error': str(e.orig)}), 405

    return '{"message": "random user created!"}', 201



@app.route('/api/v1/user/<user_id>/set-password', methods=['PUT'])
def api_single_user_set_password(user_id):
    if not user_id and type(user_id) is int and 0 < user_id:
        return '', 400

    db_query = db.session.query(User)
    db_obj = db_query.get(user_id)
    if not db_obj:
        return '', 404

    req_data = None
    if request.content_type == 'application/json':
        req_data = request.json
    elif request.content_type == 'application/x-www-form-urlencoded':
        req_data = request.form

    db_obj.password = generate_password_hash(req_data['password'])
    try:
        db.session.commit()
    except IntegrityError as e:
        return jsonify({'error': str(e.orig)}), 405

    return '', 204


@app.route('/api/v1/user/login', methods=['POST'])
def api_single_user_login():
    if request.method == 'POST':
        req_data = None
        if request.content_type == 'application/json':
            req_data = request.json
        elif request.content_type == 'application/x-www-form-urlencoded':
            req_data = request.form
        if not req_data['phone'] and not req_data['password']:
            return '', 400
        phone = req_data['phone']
        password = req_data['password']

    else:
        return '', 400

    db_query = db.session.query(User)
    try:
        db_obj = db_query.filter_by(phone=phone).one()
    except NoResultFound:
        return '', 404

    if not check_password_hash(db_obj.password, password):
        return '', 401

    identity = phone
    access_token = create_access_token(identity=identity)
    refresh_token = create_refresh_token(identity=identity)

    response = jsonify({'login': True, 'phone': phone, 'id': db_obj.id, 'access_token': access_token})
    set_access_cookies(response, access_token)
    set_refresh_cookies(response, refresh_token)

    return response, 201


@app.route('/api/v1/user/logout', methods=['POST'])
@jwt_required(optional=True)
def api_single_user_logout():
    identity = get_jwt_identity()
    db_query = db.session.query(User)
    try:
        db_obj = db_query.filter_by(phone=identity).one()
    except NoResultFound:
        return '', 400

    response = jsonify({'logout': True, "msg": "see ya again"})
    unset_jwt_cookies(response)
    return response, 201


@app.route('/api/v1/user/profile', methods=['GET'])
@jwt_required()
def api_single_user_profile():
    phone = get_jwt_identity()

    db_query = db.session.query(User)
    try:
        obj = db_query.filter_by(phone=phone).one()
    except NoResultFound:
        return '', 404

    response_obj = [dict(
        id=obj.id,
        phone=obj.phone,
        user_name=obj.user_name,
        time_account=obj.time_account,
    )]

    response = jsonify(response_obj)
    return response, 200


# Vytvor zoznam sluzieb ktory odfiltruje zoznam sluzieb podla konkretneho poskytovatela:
@app.route('/api/v1/services-user/<user_id>', methods=['GET'])
def api_service_user_id(user_id):
    sort_field, sort_dir, valid = record_sort_params_handler(request.args, Service)

    if not valid:
        return '', 400

    db_objs = get_all_db_objects(sort_field, sort_dir, db.session.query(Service)).all()

    db_query = db.session.query(User)
    obj = db_query.get(user_id)

    if obj.id != user_id:
        return '{"Message": "tak takto nie!"}', 404

    if len(db_objs):
        response_obj = []
        for x in db_objs:
            if x.user_id == obj.id:
                response_obj.append(dict(
                    id=obj.id,
                    title=x.title,
                    user_name=obj.user_name
            ))
        return jsonify(response_obj), 200
    else:
        return '', 404


# Vyhladaj z databazy userov podla user_name!!!
@app.route('/api/v1/find_user', methods=['GET'])
def find_user():
    sort_field, sort_dir, valid = record_sort_params_handler(request.args, User)
    if not valid:
        return '', 400

    search = request.args.get('search')
    user_model = db.session.query(User)

    db_find = user_model.filter(User.user_name.like(search+'%'))

    if sort_field and sort_dir:
        db_obj = db_find.order_by(text(sort_field + ' ' + sort_dir)).all()
    else:
        db_obj = db_find.all()

    res = []
    if len(search) >= 0:
        for x in db_obj:
            res.append(dict(
                id=x.id,
                phone=x.phone,
                user_name=x.user_name,
                time_account=x.time_account
            ))
        return jsonify(res)

    return {"message": "whops something went wrong!"}, 400


# Funkcia na kontrolu ci je telefonne cislo v databaze
@app.route('/api/v1/user/phone', methods=['POST'])
def get_number():
    db_obj = db.session.query(User)

    req_data = None
    if request.content_type == 'application/json':
        req_data = request.json
    elif request.content_type == 'application/x-www-form-urlencoded':
        req_data = request.form

    cislo = req_data['phone']

    for num in db_obj:
        if num.phone == cislo:
            return jsonify(num.phone), 200

    return jsonify("Number not found"), 404

