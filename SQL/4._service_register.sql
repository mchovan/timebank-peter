USE Timebank;
CREATE TABLE Serviceregister
(id INT PRIMARY KEY AUTO_INCREMENT,
service_id INT NOT NULL,
CONSTRAINT `fk_serviceregister_service`
    FOREIGN KEY (service_id) REFERENCES Service (id)
    ON DELETE CASCADE
    ON UPDATE RESTRICT,
consumer_id INT NOT NULL,
CONSTRAINT `fk_serviceregister_consumer`
    FOREIGN KEY (consumer_id) REFERENCES User (id)
    ON DELETE CASCADE
    ON UPDATE RESTRICT,
hours INT,
service_status ENUM('inprogress','ended') NOT NULL,
end_time DATE);